import http from 'http'
import { resolve as resolvePath } from 'path'
import express from 'express'
import webpack from 'webpack'
import app from './src/app'

const server = express()
const port = process.env.PORT || 3000

server.set( 'port', port )

if ( process.env.NODE_ENV === 'development' ) {
    const config = require( './webpack.dev.config' )
    const bundler = webpack( config )

    /* ----- browser-sync config ----- */
    // const bs = require( 'browser-sync' ).create()

    // bs.init({
    //     open: process.argv[2] === '--open' ? true : false,
    //     logFileChanges: true,
    //     host: 'project.local.dev',
    //     proxy: {
    //         target: 'http://localhost:' + port,
    //     },
    //     middleware: [
    //         require( 'webpack-dev-middleware' )( bundler, {
    //             publicPath: config.output.publicPath,
    //             stats: {
    //                 // assets: false,
    //                 colors: true,
    //                 // version: false,
    //                 hash: false,
    //                 timings: true,
    //                 chunks: false,
    //                 chunkModules: false,
    //                 modules: false
    //             }
    //         }),
    //         require( 'webpack-hot-middleware' )( bundler )
    //     ]
    // })

    // bs.watch(
    //   [
    //       resolvePath(__dirname, '../src/views') + '/**/*.css',
    //       resolvePath(__dirname, '../src/assets') + '/**/*.js',
    //   ], {ignored: '*.map.css'})

    /* ----- end browser-sync config ----- */

    /* ----- express config ----- */
    server.use( require( 'webpack-dev-middleware' )( bundler, {
        noInfo: true,
        publicPath: config.output.publicPath,
        stats: {
            assets: false,
            colors: true,
            version: false,
            hash: false,
            timings: false,
            chunks: false,
            chunkModules: false
        }
    }))

    server.use( require( 'webpack-hot-middleware' )( bundler ))
    /* ----- end express config ----- */

    server.use( express.static( resolvePath( __dirname, 'src' )))
} else if ( process.env.NODE_ENV === 'production' ) {
    server.use( express.static( resolvePath( __dirname, 'dist' )))
}

server.get( '/', ( req, res ) => {

    http.request( 'http://www.capital.fr/json', response => {
        const data = []
        response.on( 'data', chunk => data.push( chunk ))
        response.on( 'end', () => {
            app( req, res, data.join(''))
        })
    }).end()
})

server.listen( port, err => {
    if ( err ) {
        console.error( err )
    } else {
        console.log( 'Listening at http://localhost:' + port )
    }
})
