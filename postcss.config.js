const path = require('path');

module.exports = {
  plugins: [
    require('postcss-import')({
        path: path.resolve(__dirname, './src/assets/styles') // Du to some bug with resolve of relative/absolute path we need to define it here ATM
    }),
    require('postcss-url')(),
    require('postcss-cssnext')({
        browsers: ['> 1%', 'last 2 versions', 'Firefox ESR', 'Opera 12.1']
    }),
    require('css-mqpacker')({
        sort: true
    })
  ]
}

