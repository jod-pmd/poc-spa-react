# Isomorphic/universal boilerplate with react, webpack and express

Greatly inspired by [this very detailed post](https://medium.com/@justinjung04/react-server-side-rendering-and-hot-reloading-ffb87ca81a89).

## Install
```
$ git clone git@bitbucket.org:prismamediadigital/poc-spa-react.git
$ cd poc-spa-react
$ npm install
```

## Commmands

### Run the app from a dev server
```
$ npm start
```

### Build the app
```
$ npm run build
```

### Host the app from a server (app must have been built)
```
$ npm run serve
```

## Other resources

- [Server side rendering with React and Express](https://medium.com/front-end-hacking/server-side-rendering-with-react-and-express-382591bfc77c)
- [You Might Not Need Redux](https://medium.com/@dan_abramov/you-might-not-need-redux-be46360cf367)
