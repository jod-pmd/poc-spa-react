import React from 'react'
import { renderToString } from 'react-dom/server'
import App from './components/home'

export default ( req, res, data ) => {

    const initialState = JSON.parse( data ).editorsChoice
    const { meta } = JSON.parse( data )

    if ( process.env.NODE_ENV === 'development' ) {
        res.status(200).send(`
            <!doctype html>
            <html class="no-js" lang="fr">
                <head>
                    <script>window.__APP_INITIAL_STATE__ = ${ JSON.stringify( initialState )}</script>
                    <meta charset="utf-8">
                    <meta http-equiv="x-ua-compatible" content="ie=edge">
                    <title>${ meta.title }</title>
                    <meta name="description" content="${ meta.meta_desc }">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/10up-sanitize.css/4.1.0/sanitize.min.css">
                </head>
                <body>
                    <div id="root"></div>
                    <script src="/bundle.js"></script>
                </body>
            </html>
        `)
    } else if ( process.env.NODE_ENV === 'production' ) {
        res.status(200).send(`
            <!doctype html>
            <html class="no-js" lang="fr">
                <head>
                    <script>window.__APP_INITIAL_STATE__ = ${ JSON.stringify( initialState )}</script>
                    <meta charset="utf-8">
                    <meta http-equiv="x-ua-compatible" content="ie=edge">
                    <title>${ meta.title }</title>
                    <meta name="description" content="${ meta.meta_desc }">
                    <meta name="viewport" content="width=device-width, initial-scale=1">
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/10up-sanitize.css/4.1.0/sanitize.min.css">
                    <link rel="stylesheet" href="bundle.css" />
                </head>
                <body>
                    <div id="root">${ renderToString(
                        <App { ...initialState } />
                    )}</div>
                    <script src="bundle.js"></script>
                </body>
            </html>
        `)
    }
}
