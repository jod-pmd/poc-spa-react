import React from 'react'
import { render } from 'react-dom'

import App from './components/home'

if ( process.env.WEBPACK ) require( './assets/styles/main.css' )

render(
    <App { ...window.__APP_INITIAL_STATE__ } />,
    document.getElementById( 'root' )
)
