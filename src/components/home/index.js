import React from 'react'
import Element from '../element'

if ( process.env.WEBPACK ) require( './index.css' )

export default ( props ) => {

    const list = []

    for ( let i = 0, el; el = props[i]; i++ ) {
        list.push( <Element name={ el.name } /> )
    }

    return (
        <div>
            <h1>CAPITAL</h1>
            <h2>Sélection de la rédac</h2>
            <ul>{ list }</ul>
        </div>
    )
}
