const webpack = require( 'webpack' )
const path = require( 'path' )
const ExtractTextPlugin = require( 'extract-text-webpack-plugin' )
const BabiliPlugin = require( 'babili-webpack-plugin' )
const HtmlWebpackPlugin = require( 'html-webpack-plugin' )

module.exports = {
    context: path.resolve( __dirname, 'src' ),
    entry: {
        main: path.resolve( __dirname, 'src' )
    },
    output: {
        path: path.resolve( __dirname, 'dist' ),
        // filename: 'bundle.js?[chunkhash]',
        filename: 'bundle.js',
        publicPath: '/'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /(node_modules)/,
            use: {
                loader: 'babel-loader'
            }
        },
        {
            test: /\.css$/,
            exclude: /node_modules/,
            use: ExtractTextPlugin.extract({
                fallback: 'style-loader',
                use: [{
                    loader: 'css-loader',
                    options: {
                        importLoaders: 1,
                        minimize: true || { /* CSSNano Options */ }
                    }
                },
                {
                    loader: 'postcss-loader'
                }]
            })
        },
        {
            test: /\.(jpe?g|png|gif|svg)$/i,
            use: [{
                loader: 'file-loader',
                options: {
                    name: 'img/[name].[ext]'
                }
            },
            {
                loader: 'image-webpack-loader',
                query: {
                    mozjpeg: {
                        progressive: true,
                    },
                    gifsicle: {
                        interlaced: false,
                    },
                    optipng: {
                        optimizationLevel: 4,
                    },
                    pngquant: {
                        quality: '75-90',
                        speed: 3,
                    }
                }
            }]
        }]
    },
    plugins: [
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify( 'development' ),
                WEBPACK: true
            }
        }),
        new ExtractTextPlugin({
            filename: 'bundle.css',
            allChunks: true
        }),
        // new webpack.optimize.CommonsChunkPlugin({
        //     minChunks: 3,
        //     name: "common"
        // }),
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        new BabiliPlugin({
            // here you can configure Babili options
            // https://github.com/babel/babili/tree/master/packages/babel-preset-babili#options
        }, {
            // Here you can overide some config :
            // https://github.com/webpack-contrib/babili-webpack-plugin#overrides
        })
    ],
    resolve: {
        extensions: ['.js', '.json', '.css']
    }
}
