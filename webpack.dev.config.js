import webpack from 'webpack'
import path from 'path'

module.exports = {
    devtool: '#eval',
    context: path.resolve( __dirname, 'src' ),
    entry: [
        'webpack/hot/dev-server',
        'webpack-hot-middleware/client?reload=true',
        path.resolve( __dirname, 'src' )
    ],
    output: {
        path: path.resolve( __dirname, 'src' ),
        filename: 'bundle.js',
        publicPath: '/'
    },
    module: {
        rules: [{
            test: /\.js$/,
            exclude: /(node_modules)/,
            use: {
                loader: 'babel-loader'
            }
        },
        {
            test: /\.css$/,
            exclude: /(node_modules)/,
            use: [
                'style-loader',
                {
                    loader: 'css-loader',
                    options: {
                        importLoaders: 1
                    }
                },
                {
                    loader: 'postcss-loader'
                }
            ]
        },
        {
            test: /\.(jpe?g|png|gif|svg)$/i,
            exclude: /(fonts|node_modules)/,
            loader: 'file-loader',
            options: {
                name: 'img/[name].[ext]'
            }
        }]
    },
    plugins: [
        new webpack.HotModuleReplacementPlugin(),
        new webpack.NoEmitOnErrorsPlugin(),
        new webpack.DefinePlugin({
            'process.env': {
                NODE_ENV: JSON.stringify( 'development' ),
                WEBPACK: true
            }
        }),
        // needed for internal dependency, but need expose in config.js for external call
        new webpack.ProvidePlugin({
            $: "jquery",
            jQuery: "jquery"
        }),
        // new webpack.optimize.CommonsChunkPlugin({
        //     name: "common",
        //     minChunks: 3
        // })
    ],
    resolve: {
        extensions: ['.js', '.json', '.css']
    }
}
